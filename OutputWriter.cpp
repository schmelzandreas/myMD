//
// Created by andi on 18.12.16.
//

#include <fstream>
#include <iomanip>
#include "OutputWriter.h"


OutputWriter::OutputWriter(Cell *cells, int numCells):
        numCells(numCells), cells(cells) {
#ifdef NO_OUTPUT
    cout << "init output" << numCells;
#endif
}
void OutputWriter::write(string filename, bool excludeBoundary) {
    cout << "will write file"<<filename;
    ofstream myfile;
    typedef std::vector<Particle>::iterator iter;

    int sum = 0;
    for (int i = 0; i < numCells; i++) {
        if (cells[i].type != CELL_BOUNDARY || !excludeBoundary)
            sum += cells[i].particles.size();
    }


    cout << " writes "<< sum <<" particles" << endl;
    myfile.open (filename);
    myfile << std::setprecision(10);
    myfile << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>" << endl
           << "<VTKFile byte_order=\"LittleEndian\" type=\"UnstructuredGrid\" version=\"0.1\">" << endl
           << "  <UnstructuredGrid>" << endl
           << "    <Piece NumberOfCells=\"0\" NumberOfPoints=\""<< sum << "\">" << endl
           << "      <PointData>" << endl
           << "        <DataArray Name=\"mass\" NumberOfComponents=\"1\" format=\"ascii\" type=\"Float32\">" << endl
           << "          ";
    for (int i = 0; i < numCells; i++) {
        const iter end = cells[i].particles.end();
        if (cells[i].type != CELL_BOUNDARY || !excludeBoundary) {
            for (iter it = cells[i].particles.begin(); it != end; ++it) {
                myfile << 1 << " ";
            }
        }
    }
    myfile << endl
           << "        </DataArray>" << endl
           << "        <DataArray Name=\"cid\" NumberOfComponents=\"1\" format=\"ascii\" type=\"Float32\">" << endl
           << "          ";
    for (int i = 0; i < numCells; i++) {
        if (cells[i].type != CELL_BOUNDARY || !excludeBoundary) {
            const iter end = cells[i].particles.end();
            for (iter it = cells[i].particles.begin(); it != end; ++it) {
                myfile << i << " ";
            }
        }
    }
    myfile << endl
           << "        </DataArray>" << endl
           << "        <DataArray Name=\"cell_type\" NumberOfComponents=\"1\" format=\"ascii\" type=\"Float32\">" << endl
           << "          ";
    for (int i = 0; i < numCells; i++) {
        if (cells[i].type != CELL_BOUNDARY || !excludeBoundary) {
            const iter end = cells[i].particles.end();
            for (iter it = cells[i].particles.begin(); it != end; ++it) {
                myfile << cells[i].type << " ";
            }
        }
    }
    myfile << endl
           << "        </DataArray>" << endl
           << "        <DataArray Name=\"forces\" NumberOfComponents=\"3\" format=\"ascii\" type=\"Float32\">" << endl
           << "          ";
    for (int i = 0; i < numCells; i++) {
        if (cells[i].type != CELL_BOUNDARY || !excludeBoundary) {
            const iter end = cells[i].particles.end();
            for (iter it = cells[i].particles.begin(); it != end; ++it) {
                myfile << it->f[0] << " " << it->f[1] << " " << it->f[2] << " ";
            }
        }
    }
    myfile << endl
           << "        </DataArray>" << endl
           << "        <DataArray Name=\"velocity\" NumberOfComponents=\"3\" format=\"ascii\" type=\"Float32\">" << endl
           << "          ";
    for (int i = 0; i < numCells; i++) {
        if (cells[i].type != CELL_BOUNDARY || !excludeBoundary) {
            const iter end = cells[i].particles.end();
            for (iter it = cells[i].particles.begin(); it != end; ++it) {
                myfile << it->vel[0] << " " << it->vel[1] << " " << it->vel[2] << " ";
            }
        }
    }
    myfile << endl
           << "        </DataArray>" << endl
           << "        <DataArray Name=\"position\" NumberOfComponents=\"3\" format=\"ascii\" type=\"Float32\">" << endl
           << "          ";
    for (int i = 0; i < numCells; i++) {
        if (cells[i].type != CELL_BOUNDARY || !excludeBoundary) {
            const iter end = cells[i].particles.end();
            for (iter it = cells[i].particles.begin(); it != end; ++it) {
                myfile << it->pos[0] << " " << it->pos[1] << " " << it->pos[2] << " ";
            }
        }
    }
    myfile << endl
           << "        </DataArray>" << endl
           << "      </PointData>" << endl
           << "      <CellData/>" << endl
           << "      <Points>" << endl
           << "        <DataArray Name=\"points\" NumberOfComponents=\"3\" format=\"ascii\" type=\"Float32\">" << endl
           << "          ";
    for (int i = 0; i < numCells; i++) {
        if (cells[i].type != CELL_BOUNDARY || !excludeBoundary) {
            const iter end = cells[i].particles.end();
            for (iter it = cells[i].particles.begin(); it != end; ++it) {
                myfile << it->pos[0] << " " << it->pos[1] << " " << it->pos[2] << " ";
            }
        }
    }
    myfile << endl
           << "        </DataArray>" << endl
           << "      </Points>" << endl
           << "      <Cells>" << endl
           << "        <DataArray Name=\"types\" NumberOfComponents=\"0\" format=\"ascii\" type=\"Float32\"/>" << endl
           << "      </Cells>" << endl
           << "    </Piece>" << endl
           << "  </UnstructuredGrid>" << endl
           << "</VTKFile>" << endl;

    myfile.close();
}