#include <cstdlib>
#include <iostream>
#include <cmath>
#include <chrono>
#include <bitset>
#include <cassert>
#include <mpi.h>
#include <unistd.h>

#include "Domain.h"
#include "lib/tinyxml2.h"
#include "InputReader.h"

using namespace std;
using namespace std::chrono;

// icpc small_test.cpp -Ofast -march=native -std=c++11 -qopt-report-phase=vec -qopt-report=5 -qopt-report-file=stdout -o IntelOfast
// g++ -O0 -std=c++11
// debug mpirun -np 16 xterm -e gdb -ex run ./MolSim
#define NUM 1240000
//

high_resolution_clock::time_point t1, t2;
long dur;

int main(int argc, char* argsv[]) {
    string inputFile = "InputFiles/";
    if (argc != 2) {
        cout << "usage: MolSim filePath (basedir InputFiles/)" << endl;
        cout << "for now hardcoded one input file";
        inputFile += "test_tiny.xml";
        //exit(-1);
    } else {
        inputFile += argsv[1];
    }
    InputReader in = InputReader(inputFile);
    cout << "MolSim " << endl;
   //return 0;
#if NDEBUG
    cout << "Production version, no output file will be written" << endl;
#endif

    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);
#ifndef NO_OUTPUT
    cout << "started on " << processor_name << ", this is rank " << world_rank << " of " << world_size << endl;
#endif
    double cuts = log2 (world_size);
    if ((int) cuts != cuts) {
        cerr << "Program must run on numProcs of the power of two" << endl;
        exit(0);
    }

    Domain* domain = new Domain(&in, world_rank, world_size);
    int numIter = (int) (in.end_time / in.delta_t) * world_size;
#ifdef NO_OUTPUT
    cout << "compiled with NO_OUTPUT" << endl;
#else
    cout << "compiled with OUTPUT, will report progress" << endl;
#endif

    t1 = high_resolution_clock::now();
    domain->calculateF();
    domain->calculateF();
    t2 = high_resolution_clock::now();
    dur = duration_cast<microseconds>( t2 - t1 ).count();
    if (world_rank == 0)
        cout << "2 iter calcF took" << (dur / 1000) << "ms estimated duration for " << numIter << " iterations is " << (dur / 2 * numIter / 1000 / 1000.0) << "s" << endl;

    t1 = high_resolution_clock::now();

    for (int i = 0; i < numIter; i++) {
#ifndef NO_OUTPUT
        cout << "== Iter " << i << " ==" << endl;
#endif
        domain->calculateX(in.delta_t);
        domain->calculateF();
        domain->calculateV(in.delta_t);
        if (in.thermostat_active && i % in.n_thermostat == 0) {
            domain->applyThermostat(in.init_temp + (double)(in.target_temp - in.init_temp) * i / numIter);
        }

#if !NDEBUG
        if (i % in.writefrequency == 0) domain->plotParticles(i / in.writefrequency, in.outputname);
#endif
    }

    t2 = high_resolution_clock::now();
    dur = duration_cast<microseconds>( t2 - t1 ).count();
    if (world_rank == 0) {
        cout << "ON: " << world_size << " procs, duration : " << (dur / 1000) << "ms" <<  endl;
        cout << "that's " << (dur / numIter / 1000) << "ms per iteration" << endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    delete domain;
    cout << endl;
    return 0;
}