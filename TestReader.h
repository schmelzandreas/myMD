//
// Created by andi on 27.01.17.
//

#ifndef MYMD_TESTREADER_H
#define MYMD_TESTREADER_H

#include "Cell.h"
#include "lib/tinyxml2.h"
using namespace std;
using namespace tinyxml2;

class TestReader {
public:
    TestReader(Cell *cells, int numCells);
    void readFile(string filename, bool excludeBoundary);
    Cell* cells;
    const int numCells;

    void verifyForces(const XMLElement* forceEl, int numParticles);
    void verifyPositions(const XMLElement* positionEl, int numParticles);
    bool find(double x, double y, double z, double* arr, int numParticles);
private:
    static constexpr double MAX_ERR = 1E-8;
};


#endif //MYMD_TESTREADER_H
