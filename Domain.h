
#include "Cell.h"
#include "Vector.h"
#include "OutputWriter.h"
#include "InputReader.h"

using namespace utils;
// Symmetric neighbor definition
const char DIR[26][3] = {   // MUST NOT BE CHANGED, order is important
    {-1,-1,-1 },
    { 0,-1,-1 },
    { 1,-1,-1 },
    {-1, 0,-1 },
    { 0, 0,-1 },
    { 1, 0,-1 },
    {-1, 1,-1 },
    { 0, 1,-1 },
    { 1, 1,-1 },
    {-1,-1, 0 },
    { 0,-1, 0 },
    { 1,-1, 0 },
    {-1, 0, 0 },
    { 1, 0, 0 },
    {-1, 1, 0 },
    { 0, 1, 0 },
    { 1, 1, 0 },
    {-1,-1, 1 },
    { 0,-1, 1 },
    { 1,-1, 1 },
    {-1, 0, 1 },
    { 0, 0, 1 },
    { 1, 0, 1 },
    {-1, 1, 1 },
    { 0, 1, 1 },
    { 1, 1, 1 }
};
class Domain {
public:
    Domain(InputReader * in, int world_rank, int world_size);
    ~Domain();

    void plotParticles(int iter, string outFilename);
    void calculateF();
    void calculateX(double dt);
    void calculateV(double dt);
    void applyThermostat(double temp);
private:
    int numCells;
    double cutOffRadius;
    int world_rank, world_size;
    int domainSize[3], globalDomainSize[3], low_dim[3], high_dim[3], num_procs[3], proc_idx[3];
    int neighborPid[26];
    vector<double> recvBuffer[26];
    vector<double> sendBuffer[26];

    utils::Vector<int, 3> containerSize;// domainsize + halo

    int factor_y, factor_z; // the offsets along those axis including the halo offset
    Cell* cells;
    int getInnerCellIdx(int x, int y, int z);
    int getCellIdx(int x, int y, int z);
    int getCellIdxFromPos(double x, double y, double z);
    int getPidofProc(int x, int y, int z);
    int getCellIdxFromPos(utils::Vector<double, 3> pos);
    bool isPosInside(double x, double y, double z);
    bool isPosInDomain(double x, double y, double z);
    // the converted offsets, attention! you may only use these offsets from owncells, not from foreign (halo) cells without additional constrain check
    int neighbor_offset[26];
    OutputWriter* outputWriter;

    int *surfaceCids;
    int numSurface;
};