# This is a makefile template


# Compiler
# --------
CC=mpiCC


SOURCES = MolSim.cpp\
Domain.cpp\
Cell.cpp\
OutputWriter.cpp\
InputReader.cpp\
lib/tinyxml2.cpp\
TestReader.cpp

# Compiler flags
# -------------------------------------------------------------------------
CFLAGS= -O0 -std=c++11 -g -pg

# Linker flags
# ------------
#LDFLAGS= -lxerces-c -lcppunit -llog4cxx -fopenmp
 #-pg for debugging
LDFLAGS= -g -pg -cxx=icpc

INCLUDES=

OBJECTS=$(SOURCES:.cpp=.o)

EXECUTABLE=MolSim

PROD_CFLAGS = -cxx=icpc -Ofast -march=native -DNDEBUG -std=c++11 -c

ifeq ($(NO_OUTPUT), 1)  #at this point, the makefile checks if FEATURE is enabled
	PROD_CFLAGS = -cxx=icpc -Ofast -march=native -DNDEBUG -std=c++11 -c -DNO_OUTPUT
endif


all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@ 
		
production:
	mpiCC $(PROD_CFLAGS) MolSim.cpp -o MolSim.o
	mpiCC $(PROD_CFLAGS) Domain.cpp -o Domain.o
	mpiCC $(PROD_CFLAGS) Cell.cpp -o Cell.o
	mpiCC $(PROD_CFLAGS) OutputWriter.cpp -o OutputWriter.o
	mpiCC $(PROD_CFLAGS) InputReader.cpp -o InputReader.o
	mpiCC $(PROD_CFLAGS) lib/tinyxml2.cpp -o lib/tinyxml2.o
	mpiCC $(PROD_CFLAGS) TestReader.cpp -o TestReader.o
	mpiCC -cxx=icpc MolSim.o Domain.o Cell.o OutputWriter.o InputReader.o lib/tinyxml2.o TestReader.o -o MolSimProd

intel:
	icpc -Ofast -march=native -DNDEBUG -march=native -std=c++11 -qopt-report-phase=vec -qopt-report=5 -qopt-report-file=stdout -c MolSim.cpp -o MolSim.o
	icpc -Ofast -march=native -DNDEBUG -march=native -std=c++11 -qopt-report-phase=vec -qopt-report=5 -qopt-report-file=stdout -c Domain.cpp -o Domain.o
	icpc -Ofast -march=native -DNDEBUG -march=native -std=c++11 -qopt-report-phase=vec -qopt-report=5 -qopt-report-file=stdout -c Cell.cpp -o Cell.o
	icpc -Ofast -march=native -DNDEBUG -march=native -std=c++11 -qopt-report-phase=vec -qopt-report=5 -qopt-report-file=stdout -c OutputWriter.cpp -o OutputWriter.o
	icpc MolSim.o Domain.o Cell.o OutputWriter.o -o MolSimIntel

clean:
	rm $(OBJECTS)
	rm MolSimProd

.cpp.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@

