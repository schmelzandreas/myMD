//
// Created by andi on 18.12.16.
//

#ifndef OUTPUTWRITER_H
#define OUTPUTWRITER_H


#include <vector>
#include "Cell.h"

class OutputWriter {
public:
    OutputWriter(Cell* cells, int numCells);
    void write(string filename, bool excludeBoundary);
    Cell* cells;
    const int numCells;
};


#endif
