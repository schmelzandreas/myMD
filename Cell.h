#include <cstdlib>
#include <vector>
#include <bitset>

#include <string>
#include <iostream>
#include <climits>

#include "Vector.h"
#include "Particle.h"

#ifndef CELL_H_
#define CELL_H_


#define CELL_BOUNDARY 0
#define CELL_SURFACE 1
#define CELL_INNER 2

class Cell {
public:
    int type;
    std::vector<Particle> particles;
    //~Cell();
    Cell(){
        sendTo = 0;
        //containerSize = 0;
        type = CELL_BOUNDARY;
    };
    Cell(int cid,  int xIdx, int yIdx, int zIdx, int (&low_dim)[3], int (&high_dim)[3], double cutOff);
    void interact();
    void interact(Cell* other);
    void calculateX(double dt);
    double getEnergyDouble();
    void resetF();
    std::bitset<26> sendTo;
private:
    double cutOffSq, cutOff;
    int *low_dim, *high_dim;
    void calcParticleForces(Particle* p1, Particle* p2);

    //utils::Vector<int, 3> containerSize;// domainsize + halo
};

#endif