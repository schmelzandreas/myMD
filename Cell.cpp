#include <cstdlib>
#include <iostream>
#include <cmath>
#include <chrono>
#include <cassert>

#include "Cell.h"
#include "Domain.h"


using namespace std;
using namespace std::chrono;

/*
Cell::~Cell() {
    cout << "deconstr cell"<<endl;
    //delete &particles;
}
*/

Cell::Cell(int cid, int xIdx, int yIdx, int zIdx, int (&low_dim)[3], int (&high_dim)[3], double cutOff)
        :low_dim(low_dim), high_dim(high_dim) {
    this->low_dim = low_dim;
    //cout << "cid: " << cid << "high dim " << high_dim[0] << " | " << high_dim[1] << " | " << high_dim[2] << endl;
    // convert indices to global position indices
    int x = xIdx + low_dim[0] - 1,
        y = yIdx + low_dim[1] - 1,
        z = zIdx + low_dim[2] - 1;

    sendTo = 0;
    // determine which type the cell is
    if (x == low_dim[0] - 1 || x == high_dim[0] ||
        y == low_dim[1] - 1 || y == high_dim[1] ||
        z == low_dim[2] - 1 || z == high_dim[2]) {
        type = CELL_BOUNDARY;

    } else if ((x == low_dim[0] || x == high_dim[0] - 1) ||
               (y == low_dim[1] || y == high_dim[1] - 1) ||
               (z == low_dim[2] || z == high_dim[2] - 1)) {
        type = CELL_SURFACE;

        for (int dir = 0; dir < 26; dir++) {
            // determine at which neighbors i need to sent to
            // checks at every axis for 1. send orthogonal, 2. direction is lower boundary, 3. direction is upper boundary
            if ((DIR[dir][0] == 0 || x + DIR[dir][0] == low_dim[0] - 1 || x + DIR[dir][0] == high_dim[0]) &&
                (DIR[dir][1] == 0 || y + DIR[dir][1] == low_dim[1] - 1 || y + DIR[dir][1] == high_dim[1]) &&
                (DIR[dir][2] == 0 || z + DIR[dir][2] == low_dim[2] - 1 || z + DIR[dir][2] == high_dim[2])) {
                // send along this axis
                sendTo.set(dir);
            }
        }
    } else {
        type = CELL_INNER;
    }

    cutOffSq = cutOff * cutOff;
    this->cutOff = cutOff;
    //cout << "will init" << cid <<  " at " << x << " | " << y << " | " << z << endl;
}

// interact with the cell self
void Cell::interact() {
    const int size = particles.size();
    for (int i = 0; i < size - 1; i++) {
        for (int j = i + 1; j < size; j++) {
            calcParticleForces(&particles[i], &particles[j]);
        }
    }
}

// interact with another cell
void Cell::interact(Cell * other) {

    typedef std::vector<Particle>::iterator iter;
    const iter enda = particles.end();
    const iter endb = other->particles.end();
    for (iter ita = particles.begin(); ita != enda; ++ita) {
        for (iter itb = other->particles.begin(); itb != endb; ++itb) {
            calcParticleForces(&*ita, &*itb);
            // consider writing this the same way as above
        }
    }
}

void Cell::calcParticleForces(Particle* p1, Particle* p2)  {
    utils::Vector<double, 3> difference = p2->pos - p1->pos;

    double distanceSq = difference.L2NormSquared();

    assert(0 != distanceSq);
    if (cutOffSq > distanceSq) {  //TMP remove this check

        double epsilon_local = 2.0,
               sigma_localsq = 1.15 * 1.15;    // TMP hardcoded

        double factor = sigma_localsq / distanceSq;
        factor *= factor * factor; // pow 3

        //cout << "calc" << ita->pos <<" diff:  "<< difference << endl;

        factor = (factor - 2 * factor * factor) * 24 * epsilon_local / (distanceSq);

        difference *= factor;
        p1->f += difference;
        p2->f -= difference;

    }
}

void Cell::calculateX(double dt) {
    assert(type != CELL_BOUNDARY);
    const int size = particles.size();
    for (int i = 0; i < size; i++) {
        Particle* particle = &particles[i];
        // leap frop
        double factor = dt * dt / 2.0;

        utils::Vector<double, 3> dx = dt * particle->vel + factor * particle->f;
        assert(dx.L2NormSquared() < cutOffSq);  // arbitrary number, where i say it becomes unstable
        particle->pos += dx;
    }
}

void Cell::resetF() {
    if (type != CELL_BOUNDARY) {
        const int size = particles.size();
        for (int i = 0; i < size; i++) {
            particles[i].old_f = particles[i].f;
            particles[i].f[0] = 0;
            particles[i].f[1] = 0;
            particles[i].f[2] = 0;
        }
    } else {
        particles.clear();
    }
}

// returns the double amount of the current energy in this cell, I could ofc divide by 2 here to get the direct val
double Cell::getEnergyDouble() {
    double ekin = 0;
    const int size = particles.size();
    for (int i = 0; i < size; i++) {
        ekin += particles[i].vel.L2NormSquared();
    }
    return ekin;
}